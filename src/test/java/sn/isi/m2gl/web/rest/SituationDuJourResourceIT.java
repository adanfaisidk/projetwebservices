package sn.isi.m2gl.web.rest;

import sn.isi.m2gl.AppRestApp;
import sn.isi.m2gl.config.TestSecurityConfiguration;
import sn.isi.m2gl.domain.SituationDuJour;
import sn.isi.m2gl.repository.SituationCovidRepository;
import sn.isi.m2gl.service.SituationCovidService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SituationCovidResource} REST controller.
 */
@SpringBootTest(classes = { AppRestApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class SituationDuJourResourceIT {

    private static final Integer DEFAULT_NBRE_CAS = 1;
    private static final Integer UPDATED_NBRE_CAS = 2;

    private static final Integer DEFAULT_NBRE_CAS_POSITIF = 1;
    private static final Integer UPDATED_NBRE_CAS_POSITIF = 2;

    private static final Integer DEFAULT_NBRE_CAS_NEGATIF = 1;
    private static final Integer UPDATED_NBRE_CAS_NEGATIF = 2;

    private static final Integer DEFAULT_NBRE_DECES = 1;
    private static final Integer UPDATED_NBRE_DECES = 2;

    private static final Integer DEFAULT_NBRE_GUERIS = 1;
    private static final Integer UPDATED_NBRE_GUERIS = 2;

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private SituationCovidRepository situationCovidRepository;

    @Autowired
    private SituationCovidService situationCovidService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSituationCovidMockMvc;

    private SituationDuJour situationDuJour;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SituationDuJour createEntity(EntityManager em) {
        SituationDuJour situationDuJour = new SituationDuJour()
            .nbreCas(DEFAULT_NBRE_CAS)
            .nbreCasPositif(DEFAULT_NBRE_CAS_POSITIF)
            .nbreCasNegatif(DEFAULT_NBRE_CAS_NEGATIF)
            .nbreDeces(DEFAULT_NBRE_DECES)
            .nbreGueris(DEFAULT_NBRE_GUERIS)
            .date(DEFAULT_DATE);
        return situationDuJour;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SituationDuJour createUpdatedEntity(EntityManager em) {
        SituationDuJour situationDuJour = new SituationDuJour()
            .nbreCas(UPDATED_NBRE_CAS)
            .nbreCasPositif(UPDATED_NBRE_CAS_POSITIF)
            .nbreCasNegatif(UPDATED_NBRE_CAS_NEGATIF)
            .nbreDeces(UPDATED_NBRE_DECES)
            .nbreGueris(UPDATED_NBRE_GUERIS)
            .date(UPDATED_DATE);
        return situationDuJour;
    }

    @BeforeEach
    public void initTest() {
        situationDuJour = createEntity(em);
    }

    @Test
    @Transactional
    public void createSituationCovid() throws Exception {
        int databaseSizeBeforeCreate = situationCovidRepository.findAll().size();
        // Create the SituationDuJour
        restSituationCovidMockMvc.perform(post("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationDuJour)))
            .andExpect(status().isCreated());

        // Validate the SituationDuJour in the database
        List<SituationDuJour> situationDuJourList = situationCovidRepository.findAll();
        assertThat(situationDuJourList).hasSize(databaseSizeBeforeCreate + 1);
        SituationDuJour testSituationDuJour = situationDuJourList.get(situationDuJourList.size() - 1);
        assertThat(testSituationDuJour.getNbreCas()).isEqualTo(DEFAULT_NBRE_CAS);
        assertThat(testSituationDuJour.getNbreCasPositif()).isEqualTo(DEFAULT_NBRE_CAS_POSITIF);
        assertThat(testSituationDuJour.getNbreCasNegatif()).isEqualTo(DEFAULT_NBRE_CAS_NEGATIF);
        assertThat(testSituationDuJour.getNbreDeces()).isEqualTo(DEFAULT_NBRE_DECES);
        assertThat(testSituationDuJour.getNbreGueris()).isEqualTo(DEFAULT_NBRE_GUERIS);
        assertThat(testSituationDuJour.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    public void createSituationCovidWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = situationCovidRepository.findAll().size();

        // Create the SituationDuJour with an existing ID
        situationDuJour.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSituationCovidMockMvc.perform(post("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationDuJour)))
            .andExpect(status().isBadRequest());

        // Validate the SituationDuJour in the database
        List<SituationDuJour> situationDuJourList = situationCovidRepository.findAll();
        assertThat(situationDuJourList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSituationCovids() throws Exception {
        // Initialize the database
        situationCovidRepository.saveAndFlush(situationDuJour);

        // Get all the situationCovidList
        restSituationCovidMockMvc.perform(get("/api/situation-covids?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(situationDuJour.getId().intValue())))
            .andExpect(jsonPath("$.[*].nbreCas").value(hasItem(DEFAULT_NBRE_CAS)))
            .andExpect(jsonPath("$.[*].nbreCasPositif").value(hasItem(DEFAULT_NBRE_CAS_POSITIF)))
            .andExpect(jsonPath("$.[*].nbreCasNegatif").value(hasItem(DEFAULT_NBRE_CAS_NEGATIF)))
            .andExpect(jsonPath("$.[*].nbreDeces").value(hasItem(DEFAULT_NBRE_DECES)))
            .andExpect(jsonPath("$.[*].nbreGueris").value(hasItem(DEFAULT_NBRE_GUERIS)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getSituationCovid() throws Exception {
        // Initialize the database
        situationCovidRepository.saveAndFlush(situationDuJour);

        // Get the situationDuJour
        restSituationCovidMockMvc.perform(get("/api/situation-covids/{id}", situationDuJour.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(situationDuJour.getId().intValue()))
            .andExpect(jsonPath("$.nbreCas").value(DEFAULT_NBRE_CAS))
            .andExpect(jsonPath("$.nbreCasPositif").value(DEFAULT_NBRE_CAS_POSITIF))
            .andExpect(jsonPath("$.nbreCasNegatif").value(DEFAULT_NBRE_CAS_NEGATIF))
            .andExpect(jsonPath("$.nbreDeces").value(DEFAULT_NBRE_DECES))
            .andExpect(jsonPath("$.nbreGueris").value(DEFAULT_NBRE_GUERIS))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingSituationCovid() throws Exception {
        // Get the situationDuJour
        restSituationCovidMockMvc.perform(get("/api/situation-covids/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSituationCovid() throws Exception {
        // Initialize the database
        situationCovidService.save(situationDuJour);

        int databaseSizeBeforeUpdate = situationCovidRepository.findAll().size();

        // Update the situationDuJour
        SituationDuJour updatedSituationDuJour = situationCovidRepository.findById(situationDuJour.getId()).get();
        // Disconnect from session so that the updates on updatedSituationDuJour are not directly saved in db
        em.detach(updatedSituationDuJour);
        updatedSituationDuJour
            .nbreCas(UPDATED_NBRE_CAS)
            .nbreCasPositif(UPDATED_NBRE_CAS_POSITIF)
            .nbreCasNegatif(UPDATED_NBRE_CAS_NEGATIF)
            .nbreDeces(UPDATED_NBRE_DECES)
            .nbreGueris(UPDATED_NBRE_GUERIS)
            .date(UPDATED_DATE);

        restSituationCovidMockMvc.perform(put("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSituationDuJour)))
            .andExpect(status().isOk());

        // Validate the SituationDuJour in the database
        List<SituationDuJour> situationDuJourList = situationCovidRepository.findAll();
        assertThat(situationDuJourList).hasSize(databaseSizeBeforeUpdate);
        SituationDuJour testSituationDuJour = situationDuJourList.get(situationDuJourList.size() - 1);
        assertThat(testSituationDuJour.getNbreCas()).isEqualTo(UPDATED_NBRE_CAS);
        assertThat(testSituationDuJour.getNbreCasPositif()).isEqualTo(UPDATED_NBRE_CAS_POSITIF);
        assertThat(testSituationDuJour.getNbreCasNegatif()).isEqualTo(UPDATED_NBRE_CAS_NEGATIF);
        assertThat(testSituationDuJour.getNbreDeces()).isEqualTo(UPDATED_NBRE_DECES);
        assertThat(testSituationDuJour.getNbreGueris()).isEqualTo(UPDATED_NBRE_GUERIS);
        assertThat(testSituationDuJour.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSituationCovid() throws Exception {
        int databaseSizeBeforeUpdate = situationCovidRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSituationCovidMockMvc.perform(put("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationDuJour)))
            .andExpect(status().isBadRequest());

        // Validate the SituationDuJour in the database
        List<SituationDuJour> situationDuJourList = situationCovidRepository.findAll();
        assertThat(situationDuJourList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSituationCovid() throws Exception {
        // Initialize the database
        situationCovidService.save(situationDuJour);

        int databaseSizeBeforeDelete = situationCovidRepository.findAll().size();

        // Delete the situationDuJour
        restSituationCovidMockMvc.perform(delete("/api/situation-covids/{id}", situationDuJour.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SituationDuJour> situationDuJourList = situationCovidRepository.findAll();
        assertThat(situationDuJourList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
