package sn.isi.m2gl.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.isi.m2gl.web.rest.TestUtil;

public class SituationDuJourTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SituationDuJour.class);
        SituationDuJour situationDuJour1 = new SituationDuJour();
        situationDuJour1.setId(1L);
        SituationDuJour situationDuJour2 = new SituationDuJour();
        situationDuJour2.setId(situationDuJour1.getId());
        assertThat(situationDuJour1).isEqualTo(situationDuJour2);
        situationDuJour2.setId(2L);
        assertThat(situationDuJour1).isNotEqualTo(situationDuJour2);
        situationDuJour1.setId(null);
        assertThat(situationDuJour1).isNotEqualTo(situationDuJour2);
    }
}
