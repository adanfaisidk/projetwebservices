//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.02.16 à 01:40:14 PM UTC 
//


package sn.isi.m2gl.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nbrtest" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="postifcase" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="importedCase" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="death" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="recovered" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nbreCas",
    "nbreCasPositif",
    "nbreCasNegatif",
    "nbreDeces",
    "nbreGueris",
    "date"
})
@XmlRootElement(name = "getCovid19InfoResponse")
public class GetCovid19InfoResponse {

    @XmlElement(required = true)
    protected Integer nbreCas;
    @XmlElement(required = true)
    protected Integer nbreCasPositif;
    @XmlElement(required = true)
    protected Integer nbreCasNegatif;
    @XmlElement(required = true)
    protected Integer nbreDeces;
    @XmlElement(required = true)
    protected Integer nbreGueris;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar date;

    public Integer getNbreCas() {
        return nbreCas;
    }

    public void setNbreCas(Integer nbreCas) {
        this.nbreCas = nbreCas;
    }

    public Integer getNbreCasPositif() {
        return nbreCasPositif;
    }

    public void setNbreCasPositif(Integer nbreCasPositif) {
        this.nbreCasPositif = nbreCasPositif;
    }

    public Integer getNbreCasNegatif() {
        return nbreCasNegatif;
    }

    public void setNbreCasNegatif(Integer nbreCasNegatif) {
        this.nbreCasNegatif = nbreCasNegatif;
    }

    public Integer getNbreDeces() {
        return nbreDeces;
    }

    public void setNbreDeces(Integer nbreDeces) {
        this.nbreDeces = nbreDeces;
    }

    public Integer getNbreGueris() {
        return nbreGueris;
    }

    public void setNbreGueris(Integer nbreGueris) {
        this.nbreGueris = nbreGueris;
    }

    /**
     * Obtient la valeur de la propriété date.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDate() {
        return date;
    }

    /**
     * Définit la valeur de la propriété date.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDate(XMLGregorianCalendar value) {
        this.date = value;
    }

}
