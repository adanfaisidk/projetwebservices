package sn.isi.m2gl.service;

import sn.isi.m2gl.domain.SituationDuJour;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link SituationDuJour}.
 */
public interface SituationCovidService {

    /**
     * Save a situationDuJour.
     *
     * @param situationDuJour the entity to save.
     * @return the persisted entity.
     */
    SituationDuJour save(SituationDuJour situationDuJour);

    /**
     * Get all the situationCovids.
     *
     * @return the list of entities.
     */
    List<SituationDuJour> findAll();


    /**
     * Get the "id" situationCovid.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SituationDuJour> findOne(Long id);

    /**
     * Delete the "id" situationCovid.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
