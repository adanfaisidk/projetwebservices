package sn.isi.m2gl.service.impl;

import sn.isi.m2gl.domain.SituationDuJour;
import sn.isi.m2gl.service.SituationCovidService;
import sn.isi.m2gl.repository.SituationCovidRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link SituationDuJour}.
 */
@Service
@Transactional
public class SituationCovidServiceImpl implements SituationCovidService {

    private final Logger log = LoggerFactory.getLogger(SituationCovidServiceImpl.class);

    private final SituationCovidRepository situationCovidRepository;

    public SituationCovidServiceImpl(SituationCovidRepository situationCovidRepository) {
        this.situationCovidRepository = situationCovidRepository;
    }

    @Override
    public SituationDuJour save(SituationDuJour situationDuJour) {
        log.debug("Request to save SituationDuJour : {}", situationDuJour);
        return situationCovidRepository.save(situationDuJour);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SituationDuJour> findAll() {
        log.debug("Request to get all SituationCovids");
        return situationCovidRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<SituationDuJour> findOne(Long id) {
        log.debug("Request to get SituationDuJour : {}", id);
        return situationCovidRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete SituationDuJour : {}", id);
        situationCovidRepository.deleteById(id);
    }
}
