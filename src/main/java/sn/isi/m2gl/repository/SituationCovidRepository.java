package sn.isi.m2gl.repository;

import sn.isi.m2gl.domain.SituationDuJour;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the SituationDuJour entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SituationCovidRepository extends JpaRepository<SituationDuJour, Long> {
}
